package io.bekey.util;

import javax.mail.*;
import java.util.Properties;

public class GmailAPI {

    public static String checkMail()
    {
        String host = "pop.gmail.com";// change accordingly
        String mailStoreType = "pop3";
        String username = "autobekey@gmail.com";// change accordingly
        String password = "12345Test";// change accordingly
        String response = "";
        try {

            //create properties field
            Properties properties = new Properties();

            properties.put("mail.store.protocol", "pop3");
            properties.put("mail.pop3.host", host);
            properties.put("mail.pop3.port", "995");
            properties.put("mail.pop3.starttls.enable", "true");
            //Session emailSession = Session.getDefaultInstance(properties);

            Session emailSession = Session.getInstance(properties, new Authenticator() {
                protected PasswordAuthentication getPasswordAuthentication() {
                    return new PasswordAuthentication(username, password);
                }
            });

            //create the POP3 store object and connect with the pop server
            Store store = emailSession.getStore("pop3s");

            store.connect(host, username, password);

            //create the folder object and open it
            Folder emailFolder = store.getFolder("INBOX");
            emailFolder.open(Folder.READ_WRITE);

            // retrieve the messages from the folder in an array and print it
            Message[] messages = emailFolder.getMessages();
            System.out.println("messages.length---" + messages.length);

            Message message = messages[messages.length - 1];
            System.out.println("---------------------------------");
            //System.out.println("Email Number " + (i + 1));
            System.out.println("Subject: " + message.getSubject());
            System.out.println("From: " + message.getFrom()[0]);
            System.out.println("Text: " + message.getContent().toString());
            response = message.getSubject();
            message.setFlag(Flags.Flag.DELETED, true);


            //close the store and folder objects
            emailFolder.close(true);
            store.close();

        } catch (NoSuchProviderException e) {
            e.printStackTrace();
        } catch (MessagingException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return response;
    }
}
