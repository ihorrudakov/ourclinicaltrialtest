package io.bekey.util;

import java.io.IOException;
import java.util.Properties;

public class ConfigurationProfiles {
    private static final String DEBUG_PROPERTIES = "/debug.properties";

    private Properties properties;

    public ConfigurationProfiles() throws IOException {
        this(System.getProperty("application.properties", DEBUG_PROPERTIES));
    }

    public ConfigurationProfiles(String fromResource) throws IOException {
        properties = new Properties();
        properties.load(ConfigurationProfiles.class.getResourceAsStream(fromResource));
    }
    public String getProperty(String name) {
        return properties.getProperty(name);
    }

}
