package io.bekey.util;

import com.jcraft.jsch.JSch;
import com.jcraft.jsch.Session;

import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class DBConnection {
    String dbHost = "127.0.0.1";
    int dbPort = 3306;
    int nLocalPort = 3306;
    String driverName = "com.mysql.jdbc.Driver";
    public static Connection conn = null;
    Session session = null;

    public void createDBconnection() throws IOException {
        ConfigurationProfiles configurationProfiles = new ConfigurationProfiles();
        String sshHost = configurationProfiles.getProperty("sshHost");
        int sshPort = Integer.parseInt(configurationProfiles.getProperty("sshPort"));
        String sshUser = configurationProfiles.getProperty("sshUser");
        String sshPassword = configurationProfiles.getProperty("sshPassword");
        String dbuserName = configurationProfiles.getProperty("dbuserName");
        String dbpassword = configurationProfiles.getProperty("dbpassword");
        String url = "jdbc:mysql://127.0.0.1:" + nLocalPort + configurationProfiles.getProperty("dbName");
        try {
            //Set StrictHostKeyChecking property to no to avoid UnknownHostKey issue
            java.util.Properties config = new java.util.Properties();
            config.put("StrictHostKeyChecking", "no");
            config.setProperty("useUnicode","true");
            config.setProperty("characterEncoding","UTF-8");
            JSch jsch = new JSch();
            session = jsch.getSession(sshUser, sshHost, sshPort);
            session.setPassword(sshPassword);
            session.setConfig(config);
            session.connect();
            System.out.println("Connected");
            int assinged_port = session.setPortForwardingL(dbPort, dbHost, dbPort);
            System.out.println("localhost:" + assinged_port + " -> " + dbHost + ":" + dbPort);
            System.out.println("Port Forwarded");

            //mysql database connectivity
            Class.forName(driverName).newInstance();
            conn = DriverManager.getConnection(url, dbuserName, dbpassword);
            System.out.println(configurationProfiles.getProperty("dbName"));
            System.out.println("Database connection established");
            System.out.println("DONE");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void closeDBconnection() throws SQLException {
        if (conn != null && !conn.isClosed()){
            System.out.println("Closing Database Connection");
            conn.close();
        }
        if (session != null && session.isConnected()){
            System.out.println("Closing SSH Connection");
            session.disconnect();
        }
    }

    public Connection getConn() {
        return conn;
    }
}
