package io.bekey.page.admin;

import io.bekey.page.BasePage;

import static com.codeborne.selenide.Selectors.byXpath;
import static com.codeborne.selenide.Selenide.$;

public class StudiesPage extends BasePage{

    public void openGeneralTab() {
        $(byXpath("//span[contains(text(),'General')]")).click();
    }

    public void openMedicationsTab() {
        $(byXpath("//span[contains(text(),'Medications')]")).click();
    }

    public void openVisitsTab() {
        $(byXpath("//span[contains(text(),'Visits')]")).click();
    }

    public void fillGeneralTab(String name, String sponsor) {
        $(byXpath("//input[@id='name']")).setValue(name);
        $(byXpath("//input[@id='sponsor']")).setValue(sponsor);
    }

    public void fillMedicationsTab(String medicationName, String frequency, String timeToTakeHours, String timeToTakeMinutes, int n) {
        $(byXpath("//input[@placeholder='Enter Medication name'][" + n + "]")).setValue(medicationName);
        $(byXpath("//select[@formcontrolname='frequency'][" + n + "]")).selectOption(frequency);
        $(byXpath("//input[@placeholder='HH'][" + n + "]")).setValue(timeToTakeHours);
        $(byXpath("//input[@placeholder='MM'][" + n + "]")).setValue(timeToTakeMinutes);
    }

    public void addMedication() {
        $(byXpath("//button[contains(text(),'Add medication')]")).click();
    }

    public void fillVisitsTab(String visitName, String noteToCoor, String noteToPatient, int n) {
        $(byXpath("//input[@placeholder='Visit name'][" + n + "]")).setValue(visitName);
        $(byXpath("//textarea[@formcontrolname='noteToCoor'][" + n + "]")).setValue(noteToCoor);
        $(byXpath("//textarea[@formcontrolname='noteToPatient'][" + n + "]")).setValue(noteToPatient);
    }

    public void add1MoreVisit() {
        $(byXpath("//button[contains(text(),'Add 1 more Visit')]")).click();
    }
}
