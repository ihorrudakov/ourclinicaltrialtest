package io.bekey.page.admin;

import io.bekey.page.BasePage;

import static com.codeborne.selenide.Selectors.byXpath;
import static com.codeborne.selenide.Selenide.$;

public class UsersPage extends BasePage {
    public void fillUserForm(String role, String username, String firstName, String lastName,
                             String email, String mobileNumber, String phoneNumber, String dob,
                             String site, String physician) {
        $(byXpath("//select[@id='role']")).selectOption(role);

        $(byXpath("//input[@id='userName']")).setValue(username);
        $(byXpath("//input[@id='firstName']")).setValue(firstName);
        $(byXpath("//input[@id='lastName']")).setValue(lastName);

        $(byXpath("//input[@id='email']")).setValue(email);
        $(byXpath("//input[@id='mobile']")).setValue(mobileNumber);
        $(byXpath("//input[@id='phone']")).setValue(phoneNumber);

        if (!site.equals("")){
            $(byXpath("//ng-select[@id='sites']//input")).setValue(site);
            $(byXpath("//span[contains(.,'" + site + "')]")).click();
        }

        if (!physician.equals("")){
            $(byXpath("//ng-select[@id='physician']//input")).setValue(physician);
            $(byXpath("//span[contains(.,'" + physician + "')]")).click();
        }
    }
}
