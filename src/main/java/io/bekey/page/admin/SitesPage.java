package io.bekey.page.admin;

import io.bekey.page.BasePage;
import static com.codeborne.selenide.Selectors.byXpath;
import static com.codeborne.selenide.Selenide.$;

public class SitesPage extends BasePage{
    public void fillSitesForm(String name, String number, String study, String coor) {
        $(byXpath("//input[@id='name']")).setValue(name);
        $(byXpath("//input[@id='number']")).setValue(number);

        $(byXpath("//div[@class='ng-select-container']//input[@type='text']")).setValue(study);
        $(byXpath("//span[contains(.,'" + study + "')]")).click();

        $(byXpath("//div[@class='ng-select-container ng-has-value']//input[@type='text']")).setValue(coor);
        $(byXpath("//span[contains(.,'" + coor + "')]")).click();
    }
}
