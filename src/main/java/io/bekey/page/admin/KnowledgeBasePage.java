package io.bekey.page.admin;

import io.bekey.page.BasePage;

import com.codeborne.selenide.Condition;
import static com.codeborne.selenide.Selectors.byXpath;
import static com.codeborne.selenide.Selenide.*;
import java.io.File;

public class KnowledgeBasePage extends BasePage{

    public void addCategory(String name){
        $(byXpath("//i[@class='fa  fa-plus']/parent::button[1]")).click();
        $(byXpath("//input[@placeholder='Category name']")).should(Condition.exist).setValue(name);
    }

    public void editCategory(String categ, String name){
        $(byXpath("//div[@class='categories']//label[contains(text(),'" + categ + "')]")).click();
        $(byXpath("//i[@class='fa fa-pencil']/parent::button[1]")).click();
        $(byXpath("//input[@placeholder='Category name']")).should(Condition.exist).setValue(name);
    }

    public void deleteCategory(String categ){
        $(byXpath("//div[@class='categories']//label[contains(text(),'" + categ + "')]")).click();
        $(byXpath("//i[@class='fa fa-trash']/parent::button[1]")).click();
    }

    public void saveCategory(){
        $(byXpath("//div[@class='modal-content']//button[contains(text(),'Save')]")).click();
    }

    public void addEditArticle(String name, String categ){
        $(byXpath("//input[@id='article-name']")).should(Condition.exist).setValue(name);
        $(byXpath("//div[@class='categories']//label[contains(text(),'" + categ + "')]")).should(Condition.exist).click();
    }

    public void selectSites(String site){
        $(byXpath("//ng-select[contains(@class, 'article__sites')]")).click();
        $(byXpath("//ng-dropdown-panel//span[contains(.,'" + site + "')]")).should(Condition.exist).click();
    }

    public void fillTabs(String lang, String text){
        $(byXpath("//tabset//li[contains(@class, 'nav-item')]//span[contains(., '"+lang+"')]")).click();
        $(byXpath("//tab[@ng-reflect-heading='"+lang+"']//div[contains(@class, 'ql-editor')]")).should(Condition.exist).setValue(text);
    }

    public void addImage(String path){
        File file = new File(path);
        $(byXpath("//input[@type='file']")).uploadFile(file);
        sleep(5000);
    }

    public void deleteImage(){
        $(byXpath("//button[contains(@class, 'remove-img')]")).click();
    }

    public void checkChanges(String categ, String site, String lang, String text){
        $(byXpath("//form//p[contains(text(),'" + categ + "')]")).should(Condition.exist);
        $(byXpath("//ng-select//span[contains(text(),'" + site + "')]")).should(Condition.exist);
        $(byXpath("//tabset//li[contains(@class, 'nav-item')]//span[contains(., '"+lang+"')]")).click();
        $(byXpath("//tab[@ng-reflect-heading='"+lang+"']//div[contains(@class, 'ql-editor')][contains(.,'"+text+"')]")).should(Condition.exist);
    }
}
