package io.bekey.page.admin;

import io.bekey.page.BasePage;

import static com.codeborne.selenide.Selectors.byXpath;
import static com.codeborne.selenide.Selenide.$;

public class GroupsPage extends BasePage {
    public void fillGroupsForm(String name, String site, boolean chatEnabled, String physician, String patient) {
        $(byXpath("//input[@id='name']")).selectOption(name);

        $(byXpath("//ng-select[@id='sites']//input[@type='text']")).setValue(site);
        $(byXpath("//span[contains(.,'" + site + "')]")).click();

        if (chatEnabled) $(byXpath("//input[@id='chat-true']")).click();

        $(byXpath("//ng-select[@id='physicians']//input[@type='text']")).setValue(physician);
        $(byXpath("//span[contains(.,'" + physician + "')]")).click();

        $(byXpath("//ng-select[@id='patients']//input[@type='text']")).setValue(patient);
        $(byXpath("//span[contains(.,'" + patient + "')]")).click();
    }
}
