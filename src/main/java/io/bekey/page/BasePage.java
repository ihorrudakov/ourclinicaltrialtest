package io.bekey.page;

import io.bekey.util.ConfigurationProfiles;

import java.io.IOException;

import static com.codeborne.selenide.Selectors.byId;
import static com.codeborne.selenide.Selectors.byXpath;
import static com.codeborne.selenide.Selenide.$;
import static com.codeborne.selenide.Selenide.open;
import static com.codeborne.selenide.Selenide.sleep;
import static io.bekey.util.Constants.*;

public abstract class BasePage {

    public void loginToAdmin(String email, String pass) throws IOException {
        open(BASE_URL_ADMIN);
        $(byXpath("//input[@placeholder='Email']")).setValue(email);
        $(byXpath("//input[@placeholder='Password']")).setValue(pass);
        $(byXpath("//button[@type='submit']")).click();
    }

    public void loginToCoor(String email, String pass) throws IOException {
        open(BASE_URL_CORR);
        $(byXpath("//input[@placeholder='Email']")).setValue(email);
        $(byXpath("//input[@placeholder='Password']")).setValue(pass);
        $(byXpath("//button[@type='submit']")).click();
    }

    //admin panel

    public void adminStudiesPage() {
        $(byXpath("//a[contains(@href,'studies')]")).click();
    }

    public void adminSitesPage() {
        $(byXpath("//a[contains(@href,'sites')]")).click();
    }

    public void adminUsersPage() {
        $(byXpath("//a[contains(@href,'users')]")).click();
    }

    public void adminGroupsPage() {
        $(byXpath("//a[contains(@href,'groups')]")).click();
    }

    public void adminKBPage() {
        $(byXpath("//a[contains(@href,'articles')]")).click();
        sleep(5000);
    }

    //corr page

    public void coorStudiesPage() {
        $(byXpath("//a[contains(@href,'studies')]")).click();
    }

    public void coorUsersPage() {
        $(byXpath("//a[contains(@href,'users')]")).click();
    }

    public void coorGroupsPage() {
        $(byXpath("//a[contains(@href,'groups')]")).click();
    }

    public void coorKBPage() {
        $(byXpath("//a[contains(@href,'articles')]")).click();
    }

    public void addButton() {
        $(byXpath("//button[contains(text(), 'Add')]")).click();
    }

    public void saveButton() {
        $(byXpath("//button[contains(text(),'Save')]")).click();
        sleep(5000);
    }

    public void cancelButton() {
        $(byXpath("//button[contains(text(),'Cancel')]")).click();
    }

    public void yesButton() {
        $(byXpath("//button[contains(text(),'Yes')]")).click();
        sleep(5000);
    }

    public void noButton() {
        $(byXpath("//button[contains(text(),'No')]")).click();
        sleep(5000);
    }

    public void editButton(String param) {
        $(byXpath("//tr[contains(.,'" + param + "')]//i[@class='fa fa-pencil']/parent::button[1]")).click();
    }

    public void deleteButton(String param) {
        $(byXpath("//tr[contains(.,'" + param + "')]//i[@class='fa fa-trash']/parent::button[1]")).click();
    }

    public void activateButton(String param) {
        $(byXpath("//tr[contains(.,'" + param + "')]//i[@class='fa fa-repeat']/parent::button[1]")).click();
    }

    public void okButton() {
        $(byXpath("//button[contains(text(),'OK')]")).click();
        sleep(5000);
    }
}
