import com.codeborne.selenide.SelenideConfig;
import io.bekey.util.DBConnection;
import org.testng.annotations.*;

import java.io.IOException;
import java.sql.SQLException;

import static com.codeborne.selenide.Configuration.*;
import static com.codeborne.selenide.Selenide.$;
import static com.codeborne.selenide.Selenide.open;
import static com.codeborne.selenide.WebDriverRunner.getWebDriver;

public abstract class BaseTest {

    DBConnection dBconnection = new DBConnection();

    @Parameters({"browser"})
    @BeforeMethod
    public void setUp(@Optional("chrome") String driver) {
        browser = driver;
        timeout = 10000;
        browserSize = "1980x1024";
        baseUrl = "";
    }

    @BeforeSuite
    public void before() throws IOException {
        dBconnection.createDBconnection();
    }

    @AfterSuite
    public void after() throws SQLException {
        dBconnection.closeDBconnection();
    }

    @AfterMethod
    public void tearDown() {
        getWebDriver().quit();
    }
}
