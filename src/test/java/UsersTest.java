import io.bekey.page.BasePage;
import io.bekey.page.admin.UsersPage;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import java.io.IOException;

import static io.bekey.util.Constants.*;

public class UsersTest extends BaseTest {
    @DataProvider(name = "users")
    public static Object[][] users() {
        return new Object[][]{
                {"Patient", "patientTemp", "Test", "Patient", "autobekey+10@gmail.com", "380664568877",
                        "380664568866", "11-05-1994", "Auto Site", "Auto Physician"},
                {"Physician", "physicianTemp", "Test", "Physician", "autobekey+11@gmail.com", "380664568822",
                        "380664568833", "11-06-1994", "Auto Site", ""},
                {"Coor", "coorTemp", "Test", "Coor", "autobekey+12@gmail.com", "380664568888",
                        "380664568899", "11-07-1994", "", ""},
        };
    }

    @Test(dataProvider = "users")
    public void checkCRUDUserProfile(String role, String username, String fName, String lName,
                                     String email, String mobile, String phone, String dob,
                                     String site, String physician) throws IOException {
        UsersPage page = new UsersPage();
        page.loginToAdmin(ADMIN_EMAIL, ADMIN_PASS);
        page.adminUsersPage();
        page.addButton();
        page.fillUserForm(role, username, fName, lName, email, mobile, phone, dob, site, physician);
        page.saveButton();
    }
}
