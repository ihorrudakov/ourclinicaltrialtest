import io.bekey.page.BasePage;
import io.bekey.page.BasePage;
import io.bekey.page.admin.SitesPage;
import io.bekey.page.admin.UsersPage;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import java.io.IOException;

import static com.codeborne.selenide.Selenide.sleep;
import static io.bekey.util.Constants.*;

public class MainTest extends BaseTest {

    @DataProvider(name = "e2e")
    public static Object[][] e2e() {
        return new Object[][]{
                {"AutoTest Hospital", "380618747838", "Kharkiv", "61000", "Cardiology", "Pushkinska str, 80", "Ukraine",
                "AutoTest Location", "autobekey@gmail.com",
                "Master", "03/28/1980", "Cardiologist", "Test Practitioner", "Male", "Heart Failure", "Active",
                "60 minutes", ""}
        };
    }

    @Test
    public void checkFrontRequest() throws IOException {
        BasePage page = new UsersPage();
        page.loginToAdmin(ADMIN_EMAIL, ADMIN_PASS);
        sleep(3000);
    }

    @Test
    public void checkFrontRequest2() throws IOException {
        BasePage page = new UsersPage();
        page.loginToCoor(COOR_EMAIL, COOR_PASS);
        sleep(3000);
    }

    @Test
    public void checkFrontRequest3() throws IOException {
        BasePage page = new SitesPage();
    }
}
