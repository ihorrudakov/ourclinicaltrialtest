import io.bekey.page.admin.GroupsPage;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import java.io.IOException;

import static io.bekey.util.Constants.ADMIN_EMAIL;
import static io.bekey.util.Constants.ADMIN_PASS;

public class GroupsTest extends BaseTest {
    @DataProvider(name = "groups")
    public static Object[][] groups() {
        return new Object[][]{
                {"Test Group", "Auto Site", true, "Auto Physician", "Auto Patient"}
        };
    }

    @Test(dataProvider = "groups")
    public void checkCRUDGroupsProfile(String name, String site, boolean chat, String physician, String patient) throws IOException {
        GroupsPage page = new GroupsPage();
        page.loginToAdmin(ADMIN_EMAIL, ADMIN_PASS);
        page.adminGroupsPage();
        page.addButton();
        page.fillGroupsForm(name, site, chat, physician, patient);
        page.saveButton();
    }
}
