import org.testng.annotations.Test;
import io.bekey.page.admin.KnowledgeBasePage;

import java.io.IOException;

import static io.bekey.util.Constants.ADMIN_EMAIL;
import static io.bekey.util.Constants.ADMIN_PASS;

public class KnowledgeBaseTest extends BaseTest {

    @Test
    public void articles()throws IOException {
        KnowledgeBasePage page = new KnowledgeBasePage();
        page.loginToAdmin(ADMIN_EMAIL, ADMIN_PASS);
        page.adminKBPage();

      // create article
        page.addButton();
        // add category
        page.addCategory("Auto category");
        page.saveCategory();
        // fill article, choose category
        page.addEditArticle("Auto article", "Auto category");
        page.fillTabs("English", "English auto text");
        page.fillTabs("Danish", "Danish auto text");
        page.fillTabs("Hebrew", "Hebrew auto text");
        page.addImage("src/test/resources/image.jpg");
        page.saveButton();

      // edit article
        page.editButton("Auto article");
        page.addCategory("Auto category new");
        page.saveCategory();
        // edit category
        page.editCategory("Auto category", "Auto category 1");
        page.saveCategory();
        //change article text, change category
        page.addEditArticle("Auto article new","Auto category new");
        page.selectSites("Test Site");
        page.fillTabs("English", "English auto text 2");
        page.fillTabs("Danish", "Danish auto text 2");
        page.fillTabs("Hebrew", "Hebrew auto text 2");
        // change image
        page.deleteImage();
        page.addImage("src/test/resources/sample1.jpg");
        page.saveButton();

      // check that changes are saved
        page.editButton("Auto article new");
        page.checkChanges("Auto category new", "Test Site", "English","English auto text 2");
        page.checkChanges("Auto category new", "Test Site", "Danish","Danish auto text 2");
        page.checkChanges("Auto category new", "Test Site", "Hebrew","Hebrew auto text 2");
        //delete category without articles
        page.deleteCategory("Auto category 1");
        page.yesButton();
        // delete category with articles
        page.deleteCategory("Auto category new");
        page.yesButton();
        page.okButton();

      // delete article
        page.deleteButton("Auto article new");
        page.yesButton();

      // activate article
        page.activateButton("Auto article new");
        page.yesButton();
    }
}
