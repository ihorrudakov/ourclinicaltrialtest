import io.bekey.page.admin.SitesPage;
import io.bekey.page.admin.UsersPage;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import java.io.IOException;

import static io.bekey.util.Constants.*;

public class SitesTest extends BaseTest {
    @DataProvider(name = "sites")
    public static Object[][] sites() {
        return new Object[][]{
                {"Test Site", "999", "Auto Study", "autocoor"}
        };
    }

    @Test(dataProvider = "sites")
    public void checkCRUDSitesProfile(String name, String number, String study, String coor) throws IOException {
        SitesPage page = new SitesPage();
        page.loginToAdmin(ADMIN_EMAIL, ADMIN_PASS);
        page.adminSitesPage();
        page.addButton();
        page.fillSitesForm(name, number, study, coor);
        page.saveButton();
    }
}
