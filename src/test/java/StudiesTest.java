import io.bekey.page.admin.StudiesPage;
import io.bekey.page.admin.UsersPage;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import java.io.IOException;

import static io.bekey.util.Constants.ADMIN_EMAIL;
import static io.bekey.util.Constants.ADMIN_PASS;

public class StudiesTest extends BaseTest {
    @DataProvider(name = "studies")
    public static Object[][] studies() {
        return new Object[][]{
                {"Patient", "patientTemp", "Test", "Patient", "autobekey+10@gmail.com", "380664568877",
                        "380664568866", "11-05-1994", "Auto Site", "Auto Physician"}
        };
    }

    @Test(dataProvider = "studies")
    public void checkCRUDUserProfile(String name, String sponsor, String medicationName, String freq,
                                     String tttHours, String tttMinutes, String visitName, String noteToCoor,
                                     String noteToPatient, String octAgreement, String octTermsOfUseAndPP) throws IOException {
        StudiesPage page = new StudiesPage();
        page.loginToAdmin(ADMIN_EMAIL, ADMIN_PASS);
        page.adminStudiesPage();
        page.addButton();
        page.fillGeneralTab(name, sponsor);
        page.openMedicationsTab();
        page.fillMedicationsTab(medicationName, freq, tttHours, tttMinutes, 1);
        page.openVisitsTab();
        page.fillVisitsTab(visitName, noteToCoor, noteToPatient, 1);
        page.saveButton();
    }
}
